

// add delete task
let close = document.getElementsByClassName("close");
for (let i = 0; i < close.length; i++) {
    close[i].onclick = function() {
        let div = this.parentElement;
        div.remove();
    }
}


// checked task
let list = document.querySelector('ol');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);



// Add task element in task list
function newElement() {
    
    let task = document.createElement("li");
    let inputValue = document.getElementById("inputTask").value;
    let text = document.createTextNode(inputValue);
    task.appendChild(text);

    if (inputValue === '') {
        alert("Debes de completar tu campo");
    } else {
        document.getElementById("list").appendChild(task);
    }
    
    // clean input
    document.getElementById("inputTask").value = "";

    // add close btn
    let span = document.createElement("SPAN");
    let txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    task.appendChild(span);

    span.onclick = function() {
        let div = this.parentElement;
        div.remove();
    }
}